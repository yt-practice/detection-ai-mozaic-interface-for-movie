from flask import Flask, request, jsonify
from flask_socketio import SocketIO
import cv2
import base64
import tempfile
from flask_cors import CORS
from tqdm import tqdm

app = Flask(__name__, static_folder='app/build', static_url_path='')
CORS(app, resources={r"*": {"origins": "*"}})
socketio = SocketIO(app, cors_allowed_origins="*")
app.logger.setLevel('DEBUG')

# メモリ上に保持するフレームデータと縮小率
frames_data_base64 = []
resize_ratio = 0

@socketio.on('connect')
def handle_connect():
    print('Client connected')

@socketio.on('disconnect')
def handle_disconnect():
    print('Client disconnected')

def resize_frame(frame, target_length=640):
    height, width = frame.shape[:2]
    # 長辺を基準に縮小率を計算
    ratio = target_length / max(width, height)
    new_size = (int(width * ratio), int(height * ratio))
    resized_frame = cv2.resize(frame, new_size)
    return resized_frame, ratio

@app.route('/upload', methods=['POST'])
def upload_file():
    global frames_data_base64, resize_ratio

    app.logger.info('file uploading...')
    socketio.emit('uploadProgress', {
        'message': 'Uploading file..',
        'frameCount': 0,
        'currentFrameIndex': 0
    })
    file = request.files['video']

    with tempfile.NamedTemporaryFile(delete=False, suffix='.mp4') as temp_file:
        file.save(temp_file.name)
        temp_video_path = temp_file.name

    cap = cv2.VideoCapture(temp_video_path)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # 最初のフレームのみ読み込み、リサイズのための比率を取得
    success, first_frame = cap.read()
    if success:
        resized_frame, resize_ratio = resize_frame(first_frame)
        _, buffer = cv2.imencode('.jpg', resized_frame, [int(cv2.IMWRITE_JPEG_QUALITY), 50])
        base64_image = base64.b64encode(buffer).decode('utf-8')
        frames_data_base64.append(base64_image)
    else:
        cap.release()
        return 'Could not read the video', 500

    # 残りのフレームを処理
    for i in tqdm(range(1, frame_count), unit='frame', desc='Converting video to jpg'):
        success, frame = cap.read()
        if not success:
            break
        resized_frame, _ = resize_frame(frame, 640)
        _, buffer = cv2.imencode('.jpg', resized_frame, [int(cv2.IMWRITE_JPEG_QUALITY), 50])
        base64_image = base64.b64encode(buffer).decode('utf-8')
        frames_data_base64.append(base64_image)
        # 全体のフレーム数と現在のフレーム数を送る
        socketio.emit('uploadProgress', {
            'message': 'Converting..',
            'frameCount': frame_count,
            'currentFrameIndex': i
            })

    cap.release()

    return jsonify({
        'frame_count': frame_count,
        'frame_image': frames_data_base64[0] if frames_data_base64 else None,
        'resize_ratio': resize_ratio,
        'width': resized_frame.shape[1],
        'height': resized_frame.shape[0],
    })

@app.route('/frame/<int:frame_index>', methods=['GET'])
def get_frame(frame_index):
    global frames_data_base64

    if not frames_data_base64:
        return 'No video uploaded', 400
    if frame_index < 0 or frame_index >= len(frames_data_base64):
        return 'Frame index out of range', 400

    return jsonify({
        'frame_image': frames_data_base64[frame_index]
    })

@app.route('/reset', methods=['GET'])
def reset():
    global frames_data_base64, resize_ratio

    # メモリ上のデータを初期化
    frames_data_base64 = []
    resize_ratio = 0

    return jsonify({'message': 'Reset successfully'})


if __name__ == '__main__':
    # app.run(debug=True)
    socketio.run(app)