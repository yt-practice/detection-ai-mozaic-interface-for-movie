from backend.singleton.AppParamsSingleton import AppParamsSingleton
from flask import jsonify
from flask import request


def get_app_params_singleton():
    return AppParamsSingleton()

def is_frame_index_valid(frame_index, frames_list):
    return 0 <= frame_index < len(frames_list)

def reset_convert_image():
    """
    指定されたフレームの元画像を返す（モザイクをリセットしたい）
    """
    data = request.get_json()
    frame_index = data.get('frame_index')

    app_params_singleton = get_app_params_singleton()

    if not app_params_singleton.processed_frames_data_base64:
        return jsonify({'error': 'No video uploaded', 'code': 400}), 400

    if not is_frame_index_valid(frame_index, app_params_singleton.processed_frames_data_base64):
        return jsonify({'error': 'Frame index out of range', 'code': 400}), 400

    # 加工画像を元画像にリセットする
    app_params_singleton.processed_frames_data_base64[frame_index] = app_params_singleton.frames_data_base64[frame_index]
    # 加工＆マスク画像を元画像にリセットする
    app_params_singleton.processed_frames_data_base64_with_coords[frame_index] = app_params_singleton.frames_data_base64[frame_index]

    response = jsonify({
        'frame_image': app_params_singleton.processed_frames_data_base64[frame_index],
    })
    response.headers['Cache-Control'] = 'public, max-age=1800'  # 30分間キャッシュする
    return response
