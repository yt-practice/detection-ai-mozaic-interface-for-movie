import base64
import gc
import tempfile

import cv2
import numpy as np
from backend.singleton.AppParamsSingleton import AppParamsSingleton
from flask import jsonify
from flask import request
from tqdm import tqdm


def resize_and_encode_frame(frame, target_length=640):
    height, width = frame.shape[:2]
    ratio_ratio = target_length / max(width, height)
    new_size = (int(width * ratio_ratio), int(height * ratio_ratio))
    resized_frame = cv2.resize(frame, new_size)
    _, buffer = cv2.imencode('.jpg', resized_frame, [int(cv2.IMWRITE_JPEG_QUALITY), 50])
    return base64.b64encode(buffer).decode('utf-8'), resized_frame, ratio_ratio

def file_upload(app, socketio):
    app.logger.info('file uploading...')
    gc.collect() # ガベージコレクションを強制的に実行
    app_params_singleton = AppParamsSingleton()
    app_params_singleton.reset_data()

    # フロントに進捗情報を送信
    socketio.emit('uploadProgress', {
        'message': 'Uploading file..',
        'frameCount': 0,
        'currentFrameIndex': 0
    })
    file = request.files['video']

    with tempfile.NamedTemporaryFile(delete=False, suffix='.mp4') as temp_file:
        file.save(temp_file.name)
        temp_video_path = temp_file.name

    cap = cv2.VideoCapture(temp_video_path)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    for i in tqdm(range(frame_count), unit='frame', desc='Converting video to jpg'):
        success, frame = cap.read()
        if not success:
            break
        # 画像を縮小してBase64エンコード
        base64_image, resized_frame, resize_ratio = resize_and_encode_frame(frame)
        if i == 0: # もし初回なら縮小率をセット
            app_params_singleton.resize_ratio = resize_ratio

        app_params_singleton.frames_data_base64 = np.append(app_params_singleton.frames_data_base64, base64_image)
        # コピーを作成（加工修正を戻すとき用）
        app_params_singleton.processed_frames_data_base64 = np.append(app_params_singleton.processed_frames_data_base64, base64_image)
        # 座標を可視化した画像を入れる配列用にもコピー
        app_params_singleton.processed_frames_data_base64_with_coords = np.append(app_params_singleton.processed_frames_data_base64_with_coords, base64_image)

        # フロントに進捗情報を送信
        socketio.emit('uploadProgress', {
            'message': 'Converting..',
            'frameCount': frame_count,
            'currentFrameIndex': i
        })

    cap.release()

    # 配列が空かどうかを確認するために len() を使用
    app.logger.info(app_params_singleton.frames_data_base64[0])
    return jsonify({
        'frame_count': frame_count,
        'frame_image': app_params_singleton.frames_data_base64[0] if len(app_params_singleton.frames_data_base64) > 0 else None,
        'resize_ratio': app_params_singleton.resize_ratio,
        'resized_width': resized_frame.shape[1] if resized_frame is not None else None,
        'resized_height': resized_frame.shape[0] if resized_frame is not None else None,
    })
