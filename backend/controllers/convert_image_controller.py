import base64

import cv2
import numpy as np
from app import app
from backend.singleton.AppParamsSingleton import AppParamsSingleton
from flask import jsonify
from flask import request
from pydantic import ValidationError

from ..models import ImageConvertRequest

DEFAULT_MOSAIC_SIZE: int = 10

def encode_image_to_base64(img_cv2):
    """ 画像をBase64にエンコードする関数 """
    _, encoded_image = cv2.imencode('.jpg', img_cv2, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
    return base64.b64encode(encoded_image).decode()

def decode_base64_to_image(img_base64):
    """ Base64エンコードされた画像をデコードする関数 """
    img_data = base64.b64decode(img_base64)
    return cv2.imdecode(np.frombuffer(img_data, np.uint8), cv2.IMREAD_COLOR)

# モザイク処理を行う外部関数
def apply_mosaic(img_cv2, coordinates, pen_size, mosaic_size):
    mask = np.zeros(img_cv2.shape[:2], dtype=np.uint8)
    for coord in coordinates:
        x_rounded, y_rounded = int(round(coord.x)), int(round(coord.y))
        cv2.circle(mask, (x_rounded, y_rounded), int(round(pen_size // 2)), (255), -1)

    for i in range(0, mask.shape[0], mosaic_size):
        for j in range(0, mask.shape[1], mosaic_size):
            if mask[i:i+mosaic_size, j:j+mosaic_size].any():
                img_cv2[i:i+mosaic_size, j:j+mosaic_size] = cv2.blur(img_cv2[i:i+mosaic_size, j:j+mosaic_size], (mosaic_size, mosaic_size))
    return img_cv2

# モザイクとその視覚化を行う関数
def visualize_mosaic_area(img_cv2, coordinates, pen_size, mosaic_size):
    # モザイク処理
    mosaiced_img = apply_mosaic(img_cv2.copy(), coordinates, pen_size, mosaic_size)

    # マスク生成
    mask = np.zeros_like(mosaiced_img)
    for coord in coordinates:
        x_rounded, y_rounded = int(round(coord.x)), int(round(coord.y))
        cv2.circle(mask, (x_rounded, y_rounded), int(round(pen_size // 2)), (255, 255, 255), -1)

    # 半透明の青で塗りつぶす
    alpha = 0.3
    blue_overlay = np.full_like(mosaiced_img, (0, 0, 255))
    mask_blue = mask[:, :, 0] > 0
    mosaiced_img[mask_blue] = (1.0 - alpha) * mosaiced_img[mask_blue] + alpha * blue_overlay[mask_blue]

    # 輪郭線生成
    contour_layer = np.zeros_like(mosaiced_img)
    contours, _ = cv2.findContours(mask[:, :, 0], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(contour_layer, contours, -1, (255, 255, 255), 1)

    # 輪郭線を合成
    alpha_line = 0.3
    mask_line = contour_layer.any(axis=2)
    mosaiced_img[mask_line] = (1.0 - alpha_line) * mosaiced_img[mask_line] + alpha_line * contour_layer[mask_line]

    return mosaiced_img

def convert_image():
    try:
        APS = AppParamsSingleton()
        data = request.get_json()
        request_data = ImageConvertRequest(**data)

        # モザイク処理用の元画像と可視化処理用の元画像を取得
        mosaic_base_img_cv2 = decode_base64_to_image(APS.processed_frames_data_base64[request_data.frame_index])
        # モザイク処理
        mosaic_size = request_data.mosaic_size if request_data.mosaic_size else DEFAULT_MOSAIC_SIZE
        processed_img_cv2 = apply_mosaic(mosaic_base_img_cv2, request_data.coordinates, request_data.pen_size, mosaic_size)
        # Base64エンコード
        processed_image_base64 = encode_image_to_base64(processed_img_cv2)


        # モザイクされた領域に青色の半透明のレイヤーを重ねる
        visualize_base_img_cv2 = decode_base64_to_image(APS.processed_frames_data_base64_with_coords[request_data.frame_index])
        # 可視化画像の生成処理
        visualization_img_cv2 = visualize_mosaic_area(visualize_base_img_cv2, request_data.coordinates, request_data.pen_size, mosaic_size)
        # Base64エンコード
        visualization_image_base64 = encode_image_to_base64(visualization_img_cv2)

        # 結果の保存
        APS.total_coordinates_with_frame_index.add_coordinates(request_data.frame_index, request_data.coordinates)
        # モザイクした画像を保存
        APS.processed_frames_data_base64[request_data.frame_index] = processed_image_base64
        # モザイク＆可視化した画像を保存
        APS.processed_frames_data_base64_with_coords[request_data.frame_index] = visualization_image_base64

        return jsonify({
            "message": "Image processed successfully",
            "convert_image": APS.processed_frames_data_base64[request_data.frame_index]
        })

    except ValidationError as e:
        app.logger.error(f"ValidationError: {e}")
        return jsonify({"error": str(e)}), 400
    except Exception as e:
        app.logger.error(f"Unexpected error: {e}")
        return jsonify({"error": "An unexpected error occurred"}), 500