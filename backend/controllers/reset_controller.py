# リセット処理を行うコントローラー
from backend.singleton.AppParamsSingleton import AppParamsSingleton
from flask import jsonify


def reset():
    # シングルトンのインスタンスを取得
    app_params_singleton = AppParamsSingleton()
    # データを初期化
    app_params_singleton.reset_data()
    return jsonify({'message': 'Reset successfully'})
