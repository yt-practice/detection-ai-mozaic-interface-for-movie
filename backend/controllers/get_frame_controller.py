from functools import lru_cache

from backend.singleton.AppParamsSingleton import AppParamsSingleton
from flask import jsonify


@lru_cache(maxsize=None)  # キャッシュの適用
def get_frame(frame_index: int, image_type: str):

    app_params_singleton = AppParamsSingleton()
    # NumPy 配列が空かどうかをチェック
    if app_params_singleton.processed_frames_data_base64.size == 0:
        return 'No video uploaded', 400
    # フレーム番号が範囲内かどうかをチェック
    if frame_index < 0 or frame_index >= app_params_singleton.processed_frames_data_base64.size:
        return 'Frame index out of range', 400

    frame_image = None
    if image_type == 'processed':
        # TODO [ ] ここで一回目に表示したモザイクがかかってない画像が返ってしまっている
        frame_image = app_params_singleton.processed_frames_data_base64[frame_index]
    elif image_type == 'visualized':
        frame_image = app_params_singleton.processed_frames_data_base64_with_coords[frame_index]
    else:
        return 'Invalid image type', 400

    response = jsonify({
        'frame_image': frame_image,
    })
    response.headers['Cache-Control'] = 'public, max-age=1800'  # 30分間キャッシュする
    return response
