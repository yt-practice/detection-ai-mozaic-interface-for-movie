import numpy as np

from ..models import FrameIndexWithTotalCoords


class AppParamsSingleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(AppParamsSingleton, cls).__new__(cls, *args, **kwargs)
            # 縮小してBase64エンコードされた画像データ（文字列）の配列（フロントの描画用）これは描画用オリジナル配列
            # cls.frames_data_base64: list[str] = []
            cls.frames_data_base64 = np.empty((0,), dtype='object')
            # `frames_data_base64`を加工した画像を入れる配列
            # cls.processed_frames_data_base64: list[str] = []
            cls.processed_frames_data_base64 = np.empty((0,), dtype='object')
            # `frames_data_base64`の座標部分を可視化した画像を入れる配列
            # cls.processed_frames_data_base64_with_coords: list[str] = []
            cls.processed_frames_data_base64_with_coords = np.empty((0,), dtype='object')

            # 画像の縮小率
            cls.resize_ratio: int = 0
            # 書き出す際の座標情報を保持する配列
            cls.total_coordinates_with_frame_index: FrameIndexWithTotalCoords = FrameIndexWithTotalCoords(frames={})
        return cls._instance

    # データを初期化する
    def reset_data(self):
        self.frames_data_base64 = np.empty((0,), dtype='object')
        self.processed_frames_data_base64 = np.empty((0,), dtype='object')
        self.processed_frames_data_base64_with_coords = np.empty((0,), dtype='object')
        self.resize_ratio = 0
        self.total_coordinates_with_frame_index = FrameIndexWithTotalCoords(frames={})
