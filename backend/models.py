from pydantic import BaseModel


# 座標データのモデル定義
class Coordinate(BaseModel):
    x: float
    y: float
class CoordinatesList(BaseModel):
    coordinates: list[Coordinate]

# リクエストデータのモデル定義
class ImageConvertRequest(BaseModel):
    frame_index: int
    pen_size: int
    coordinates: list[Coordinate]
    mosaic_size: int

class FrameIndexWithTotalCoords(BaseModel):
    frames: dict[int, list[CoordinatesList]]

    # frame_indexを指定して座標情報を追加する処理
    def add_coordinates(self, frame_index: int, coordinates: list[Coordinate]) -> None:
        if frame_index not in self.frames:
            self.frames[frame_index] = []
        self.frames[frame_index].append(CoordinatesList(coordinates=coordinates))
