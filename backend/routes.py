from flask import Blueprint
from flask import current_app as app

from backend.socketio_instance import socketio

from .controllers.convert_image_controller import convert_image
from .controllers.file_upload_controller import file_upload
from .controllers.get_frame_controller import get_frame
from .controllers.get_frames_controller import get_frames
from .controllers.reset_controller import reset
from .controllers.reset_convert_image_controller import reset_convert_image

main_routes = Blueprint('main_route', __name__)

@main_routes.route('/', methods=['GET'])
def api_index() -> str:
    return 'working!'

# 動画系ファイルのアップロードを受け付ける
@main_routes.route('/file_upload', methods=['POST'])
def api_file_upload():
    return file_upload(app, socketio)

# 指定されたフレームの画像を返す
@main_routes.route('/frame/<int:frame_index>/image_type/<image_type>', methods=['GET'])
def api_get_frame(frame_index, image_type):
    return get_frame(frame_index, image_type)

# frame_indexのリストを受け取り、それに対応するフレームの画像を返す
@main_routes.route('/frames', methods=['POST'])
def api_get_frames():
    return get_frames()

# 読み込んだ動画や関連パラメータをリセットする
@main_routes.route('/reset', methods=['GET'])
def api_reset():
    return reset()

# 指定されたフレームに加工を施して返す
@main_routes.route('/convert_image', methods=['POST'])
def api_convert_image():
    return convert_image()

# 指定されたフレームの加工をリセットして返す
@main_routes.route('/reset_convert_image', methods=['POST'])
def api_reset_convert_image():
    return reset_convert_image()
