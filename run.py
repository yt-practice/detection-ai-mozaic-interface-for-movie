from backend import create_app
from backend.socketio_instance import socketio
from flask_cors import CORS

app = create_app()

# SocketIOのCORS設定
CORS(app, resources={r"*": {"origins": "*"}})
socketio.init_app(app, cors_allowed_origins="*")

# FlaskのCORS設定

@socketio.on('connect')
def handle_connect():
    print('Client connected')

@socketio.on('disconnect')
def handle_disconnect():
    print('Client disconnected')

if __name__ == '__main__':
    app.logger.setLevel('DEBUG')
    socketio.run(app, debug=True)
