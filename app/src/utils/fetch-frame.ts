import { GetFrameRequest } from "../types/FlaskClientTypes";
import { FlaskClient } from "./flask-client";

export const fetchFrame = async (frame_index: GetFrameRequest): Promise<string | null> => {
    try {
        const getFrameResponse = await FlaskClient.getFrame(frame_index);
        return `data:image/jpeg;base64,${getFrameResponse.data.frame_image}`;
    } catch (error) {
        console.error('Error loading frame:', error);
        return null;
    }
};