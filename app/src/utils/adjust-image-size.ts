// 描画する際の画像サイズを調整する関数
export const adjustImageSize = (
    originalWidth: number,
    originalHeight: number,
    maxWidth: number,
    maxHeight: number
) => {
    const aspectRatio = originalWidth / originalHeight;
    let adjustedWidth, adjustedHeight;
    if (originalWidth >= originalHeight) {
        adjustedWidth = maxWidth;
        adjustedHeight = maxWidth / aspectRatio;
    } else {
        adjustedHeight = maxHeight;
        adjustedWidth = maxHeight * aspectRatio;
    }
    return { width: adjustedWidth, height: adjustedHeight };
};
