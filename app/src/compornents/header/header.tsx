import React from 'react';
import styles from './header.module.sass';

export const Header: React.FC = () => {
    return (
        <header className={styles.header}>
            <div className={styles.header__logo}>
                <img src="app-logo-512.png" alt="logo" />
            </div>
            <h1>Detection AI moZaic Interface for Movie</h1>
        </header>
    )
}
export default Header;