import React, { useEffect, useState } from 'react';
import { FlaskClient } from '../../utils/flask-client';
import styles from './style.module.sass';
import { timelineThumbnailNum } from './variables';
import { GetFramesRequest } from '../../types/FlaskClientTypes'

type TimelineThumbnailsProps = {
    frameCount: number,
    setFrameIndex: React.Dispatch<React.SetStateAction<number>>;
    setFrameImageUrl: React.Dispatch<React.SetStateAction<string | null>>;
};

export const TimelineThumbnails: React.FC<TimelineThumbnailsProps> = React.memo(({
    frameCount,
    setFrameIndex,
    setFrameImageUrl,
}) => {
    const [thumbnails, setThumbnails] = useState<string[]>([]);
    // frame_index_intervalの計算
    const calculateFrameIndexInterval = (totalFrames: number): number => {
        return totalFrames < timelineThumbnailNum ? 1 : Math.ceil(totalFrames / timelineThumbnailNum);
    };
    const frameIndexInterval = calculateFrameIndexInterval(frameCount);

    useEffect(() => {
        const fetchThumbnails = async () => {
            try {
                const request: GetFramesRequest = {
                    total_frame_index: frameCount,
                    frame_index_interval: frameIndexInterval,
                };
                const timelineThumbnails = await FlaskClient.getTimelineThumbnails(request);
                const newThumbnails = timelineThumbnails.data.frame_images
                    .map((thumbnail: any) => thumbnail)
                    .filter((thumbnail: any) => thumbnail !== null)
                    .map((encodedImage: any) => `data:image/jpeg;base64,${encodedImage}`);
                setThumbnails(newThumbnails);

            } catch (error) {
                console.error('Error fetching thumbnails:', error);
            }
        };
        fetchThumbnails();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleThumbnailClick = (index: number) => {
        const frameIndex = index * frameIndexInterval;
        setFrameIndex(frameIndex);
        setFrameImageUrl(thumbnails[index]);
    };

    return (
        <div className={styles.timelineThumbnails}>
            {thumbnails.map((thumbnail, index) => (
                <img
                    key={index}
                    src={thumbnail}
                    alt={`Frame ${index * calculateFrameIndexInterval(frameCount)}`}
                    onClick={() => handleThumbnailClick(index)}
                />
            ))}
        </div>
    )
});
