import React, { useState, useEffect, useRef, useCallback } from 'react';
import { Stage, Layer, Line, Image as KonvaImage } from 'react-konva';
import Konva from 'konva';
import { adjustImageSize } from '../../utils/adjust-image-size'
import { Coordinate, maxWidth, maxHeight } from './variables';
import styles from './style.module.sass';
import { FlaskClient } from '../../utils/flask-client';
import { ConvertImageRequest } from '../../types/FlaskClientTypes';


type KonvaCanvasProps = {
    setFrameImageUrl: React.Dispatch<React.SetStateAction<string | null>>;
    setMaskCoordinates: React.Dispatch<React.SetStateAction<Coordinate[]>>;
    setImageSize: React.Dispatch<React.SetStateAction<{ width: number; height: number }>>;
    frameImageUrl: string | null;
    frameIndex: number;
    maskCoordinates: Coordinate[];
    penSize: number;
    mosaicSize: number;
    imageSize: { width: number; height: number };
    isMaskMode: boolean;
    setCssCursorDeactive: React.Dispatch<React.SetStateAction<boolean>>;
    cssCursorDeactive: boolean;
};

export const KonvaCanvas: React.FC<KonvaCanvasProps> = ({
    setFrameImageUrl,
    setMaskCoordinates,
    setImageSize,
    frameImageUrl,
    maskCoordinates,
    penSize,
    mosaicSize,
    imageSize,
    frameIndex,
    isMaskMode,
    setCssCursorDeactive,
    cssCursorDeactive
}) => {
    const stageRef = useRef<Konva.Stage>(null); // KonvaのStageへの参照
    const [cursorPos, setCursorPos] = useState({ x: -300, y: -300 }); // カーソルの座標（カーソルをcss描画しているので、カーソルの座標を管理する必要がある）
    const [konvaImage, setKonvaImage] = useState<HTMLImageElement | null>(null); // Konvaで描画する画像
    const [drawing, setDrawing] = useState<boolean>(false); // ペンによる描画中かどうか
    const [isCursorOnCanvas, setIsCursorOnCanvas] = useState<boolean>(false); // カーソルがキャンバス上にあるかどうか
    const [isSizeSet, setIsSizeSet] = useState(false);

    // フレーム画像のURLが変更されたら、Konvaで描画する画像を更新する
    useEffect(() => {
        if (frameImageUrl) {
            const image = new window.Image();
            image.onload = () => {
                setKonvaImage(image);
            };
            image.src = frameImageUrl;
        }
    }, [frameImageUrl]);

    // フレーム画像のサイズが変更されたら、Konvaで描画する画像のサイズを更新する
    const setImageSizeOnce = useCallback(() => {
      if (konvaImage && !isSizeSet) {
        console.log('aaaaa')
        const originalWidth = konvaImage.width;
        const originalHeight = konvaImage.height;
        setImageSize(adjustImageSize(originalWidth, originalHeight, maxWidth, maxHeight));
        setIsSizeSet(true);
      }
    }, [konvaImage, isSizeSet, setImageSize]);
    useEffect(() => {
      setImageSizeOnce();
    }, [setImageSizeOnce]);

    // ペンによる描画を開始する処理
    const handleMouseDown = (e: Konva.KonvaEventObject<MouseEvent>) => {
        if (isMaskMode) return; // マスクモード中は何もしない
        const stage = stageRef.current;
        if (stage) {
            const point = stage.getPointerPosition();
            if (point) {
                setDrawing(true);
                setMaskCoordinates([{ x: point.x, y: point.y }]);
            }
        }
    };

    // ペンによる描画を続ける処理
    const handleMouseMove = (e: Konva.KonvaEventObject<MouseEvent>) => {
        if (isMaskMode) return; // マスクモード中は何もしない
        if (!drawing) return;
        setCursorPos({ x: e.evt.clientX - 4, y: e.evt.clientY - 4 });
        const stage = stageRef.current;
        if (stage) {
            const point = stage.getPointerPosition();
            if (point) {
                setMaskCoordinates(prev => [...prev, point]);
            }
        }
    };

    // 座標を補間する処理
    const interpolatePoints = (point1: Coordinate, point2: Coordinate): Coordinate[] => {
        const points: Coordinate[] = [];
        const dx = point2.x - point1.x;
        const dy = point2.y - point1.y;
        const steps = Math.max(Math.abs(dx), Math.abs(dy)) || 1;
        for (let i = 0; i <= steps; i++) {
            points.push({
                x: point1.x + (dx * i) / steps,
                y: point1.y + (dy * i) / steps,
            });
        }
        return points;
    };

    // ペンによる描画を終了する処理
    const handleMouseUp = async (e: Konva.KonvaEventObject<MouseEvent>) => {
        if (isMaskMode) return; // マスクモード中は何もしない
        setDrawing(false);
        setCssCursorDeactive(true);
        let interpolatedPoints: Coordinate[] = [];
        if (maskCoordinates.length === 1) interpolatedPoints.push(maskCoordinates[0]);
        // マスクの座標を補間する処理
        for (let i = 0; i < maskCoordinates.length - 1; i++) {
            const start = maskCoordinates[i];
            const end = maskCoordinates[i + 1];
            interpolatedPoints = [...interpolatedPoints, ...interpolatePoints(start, end)];
        }

        const request: ConvertImageRequest = {
            frame_index: frameIndex,
            pen_size: penSize,
            coordinates: interpolatedPoints,
            mosaic_size: mosaicSize,
        }
        // バックエンドに座標情報を送信する処理
        const response = FlaskClient.convertImage(request);
        // バックエンドから処理済みの画像を受け取る処理
        const base64Image = (await response).data.convert_image;
        let _img = `data:image/jpeg;base64,${base64Image}`;
        setMaskCoordinates([]);
        setFrameImageUrl(_img);
        setCssCursorDeactive(false);
    };

    // カーソルの座標を更新する処理
    useEffect(() => {
        const handleMouseMove = (e: MouseEvent) => {
            if (isCursorOnCanvas) {
                setCursorPos({ x: e.clientX, y: e.clientY });
            }
        };

        window.addEventListener('mousemove', handleMouseMove);

        return () => {
            window.removeEventListener('mousemove', handleMouseMove);
        };
    }, [isCursorOnCanvas, setCursorPos]);

    // カーソルを描画する処理
    useEffect(() => {
        const customCursor = document.getElementById('customCursor');
        if (customCursor) {
            customCursor.style.transform = `translate(${cursorPos.x}px, ${cursorPos.y}px)`;
        }
    }, [cursorPos]);

    return (
        <div className={styles.konvaCanvas}>
            {isCursorOnCanvas && (
                <div
                    style={{
                        display: cssCursorDeactive ? 'none' : 'block',
                        pointerEvents: 'none',
                        position: 'fixed',
                        left: cursorPos.x,
                        top: cursorPos.y,
                        width: `${penSize}px`,
                        height: `${penSize}px`,
                        background: 'rgba(255, 255, 255, 0.2)',
                        transform: `translate(-${penSize / 2}px, -${penSize / 2}px)`,
                        borderRadius: '50%',
                        zIndex: 999,
                    }}
                />
            )}
            <Stage
                style={{ cursor: 'none', alignItems: 'center' }}
                onMouseEnter={() => setIsCursorOnCanvas(true)}
                onMouseLeave={() => setIsCursorOnCanvas(false)}
                width={imageSize.width}
                height={imageSize.height}
                onMouseDown={handleMouseDown}
                onMouseMove={handleMouseMove}
                onMouseUp={handleMouseUp}
                ref={stageRef}
            >
                <Layer>
                    <KonvaImage
                        image={konvaImage || undefined}
                        width={imageSize.width}
                        height={imageSize.height}
                    />
                    <Line
                        points={maskCoordinates.flatMap(coord => [coord.x, coord.y])}
                        stroke="rgba(255, 255, 255, 0.2)"
                        strokeWidth={penSize}
                        tension={0.0}
                        lineCap="round"
                        lineJoin="round"
                    />
                </Layer>
            </Stage>
        </div>
    )
}

