// 主に座標系の型定義
export type Coordinate = {
    x: number;
    y: number;
};
// 定数
export const maxWidth = 640; // キャンバスの最大幅
export const maxHeight = 640; // キャンバスの最大高さ

export const timelineThumbnailNum = 100; // タイムラインに表示するサムネイルの数