import { FlaskClient } from '../../utils/flask-client';
import { Coordinate } from './variables';
import styles from './style.module.sass';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashCan } from '@fortawesome/free-solid-svg-icons';
import { memo } from 'react';

type fileResetProps = {
    setFrameCount: React.Dispatch<React.SetStateAction<number>>;
    setFrameIndex: React.Dispatch<React.SetStateAction<number>>;
    setFrameImageUrl: React.Dispatch<React.SetStateAction<string | null>>;
    setImageSize: React.Dispatch<React.SetStateAction<{ width: number; height: number }>>;
    setMaskCoordinates: React.Dispatch<React.SetStateAction<Coordinate[]>>;
    setisFileSelected: React.Dispatch<React.SetStateAction<boolean>>;
}

const FileResetComponent: React.FC<fileResetProps> = ({
    setFrameCount,
    setFrameIndex,
    setFrameImageUrl,
    setImageSize,
    setMaskCoordinates,
    setisFileSelected,
}) => {
    const handleReset = async () => {
        // バックエンドのリセットエンドポイントを呼び出す
        await FlaskClient.reset();

        // フロントエンドの状態をリセットする
        setFrameCount(1);
        setFrameIndex(0);
        setFrameImageUrl(null);
        setImageSize({ width: 0, height: 0 });
        setMaskCoordinates([]);
        setisFileSelected(false);
    };

    return (
        <div className={styles.fileResetButton}>
            <button onClick={handleReset}>File Reset<FontAwesomeIcon icon={faTrashCan} /></button>
        </div>
    );
}

export const FileReset = memo(FileResetComponent);