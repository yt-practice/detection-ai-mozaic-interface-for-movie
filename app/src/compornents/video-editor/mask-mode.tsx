import React, { useEffect } from 'react';
import { GetFrameRequest } from '../../types/FlaskClientTypes';
import { fetchFrame } from '../../utils/fetch-frame';
import styles from './style.module.sass';
import { faCircleHalf } from '@fortawesome/pro-duotone-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';



type MaskModeProps = {
    frameIndex: number;
    setFrameImageUrl: React.Dispatch<React.SetStateAction<string | null>>;
    // マスクモード関連
    isQuickMaskMode: boolean;
    isMaskmodeButtonActive: boolean;
    setIsQuickMaskMode: React.Dispatch<React.SetStateAction<boolean>>;
    setIsMaskModeButtonActive: React.Dispatch<React.SetStateAction<boolean>>;
};

export const MaskMode: React.FC<MaskModeProps> = React.memo(({
    frameIndex,
    setFrameImageUrl,
    // マスクモード関連
    isQuickMaskMode,
    isMaskmodeButtonActive,
    setIsQuickMaskMode,
    setIsMaskModeButtonActive,
}) => {

    const createFetchFrameRequest = (imageType: 'processed' | 'visualized'): GetFrameRequest => {
        return {
            frame_index: frameIndex,
            image_type: imageType
        };
    }
    useEffect(() => {
        const handleKeyDown = async (e: KeyboardEvent) => {
            if ((e.key === 'q' || e.key === 'Q') && !isQuickMaskMode) {
                if (isMaskmodeButtonActive) return;
                setIsQuickMaskMode(true);
                // visualizedの画像を取得する
                setFrameImageUrl(await fetchFrame(createFetchFrameRequest('visualized')))
            }
        };
        const handleKeyUp = async (e: KeyboardEvent) => {
            if ((e.key === 'q' || e.key === 'Q') && isQuickMaskMode){
                if (isMaskmodeButtonActive) return;
                setIsQuickMaskMode(false);
                // processedの画像を取得する
                setFrameImageUrl(await fetchFrame(createFetchFrameRequest('processed')))
            }
        };
        window.addEventListener('keydown', handleKeyDown);
        window.addEventListener('keyup', handleKeyUp);
        return () => {
            window.removeEventListener('keydown', handleKeyDown);
            window.removeEventListener('keyup', handleKeyUp);
        };
    });

    // isMaskmodeButtonActiveがtrueになったら画像をマスクモード用に差し替える
    useEffect(() => {
        const fetchFrameRequest = createFetchFrameRequest(isMaskmodeButtonActive ? 'visualized' : 'processed');
        const fetchFrameUrl = async () => {
            const imageUrl = await fetchFrame(fetchFrameRequest);
            if (imageUrl) {
                setFrameImageUrl(imageUrl);
            }
        };
        fetchFrameUrl();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isMaskmodeButtonActive])

    // TODO [ ] マスクモード時に過去のモザイク処理が消えてしまっている
    //   - もういちどモザイク処理すると復活する（おそらく差し替え処理がまちがってる）
    //     - 再現できない？後で確認する
    return (
        <>
            <div
                onClick={() => {setIsMaskModeButtonActive(!isMaskmodeButtonActive)}}
                ><FontAwesomeIcon
                    icon={faCircleHalf}
                    className={isMaskmodeButtonActive || isQuickMaskMode
                        ? styles.activeMaskModeButton
                        : styles.maskModeButton
                    }
                    ></FontAwesomeIcon>
            </div>
        </>
    )
});