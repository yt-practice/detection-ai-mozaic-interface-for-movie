import { Coordinate } from '../compornents/video-editor/variables';

// 定数
export type FileUploadRequest = FormData;
export type FileUploadResponse = {
    frame_count: number;
    frame_image: string,
    resized_width: number;
    resized_height: number;
}

export type GetFrameRequest = {
    frame_index: number;
    image_type: "processed" | "visualized";
}
export type GetFrameResponse = {
    frame_image: string | null;
}

export type GetFramesRequest = {
    total_frame_index: number;
    frame_index_interval: number;
}
export type GetFramesResponse = {
    frame_images: string[];
}

export type FileUploadProgressObject = {
    message: string;
    frameCount: number;
    currentFrameIndex: number;
};

export type ResetResponse = {
    message: string;
}

export type ConvertImageRequest = {
    frame_index: number;
    pen_size: number;
    coordinates: Coordinate[];
    mosaic_size: number;
}

export type ConvertImageResponse = {
    message: string;
    convert_image: string | null;
};

export type ResetConvertImageRequest = {
    frame_index: number;
}